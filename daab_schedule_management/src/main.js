'use strict';

// モジュール間共有変数
const g = require('./config');

// module(vendor)
const _ = require('lodash');

// 作成する必要があるディレクトリがなければ作る
require('./utils/stat-mkdir').create([g.PATH_LOGS]);


const main = ({ robot, scenario, logger }) => {

  // ボットからの問い合わせ
  scenario.robotEventHandler();

  // テスト・デバッグ
  // robot.hear(/PING$/i, (res) => res.send('PONG'));
  // robot.hear(/DEBUG$/i, (res) => logger.debug('これはデバッグです。'));
  // robot.hear(/ERROR$/i, (res) => logger.error('これはエラーです。'));
}

module.exports = main;
