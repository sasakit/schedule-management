'use strict';

const fs = require('fs');


class StatMkdir {

  create(dirs = []) {
    const allDirCreate = (idx = 0) => {
      return new Promise((resolve, reject) => {
        if (!dirs[idx]) {
          return resolve(true);
        }

        this.statMkdir(dirs[idx])
          .then(() => allDirCreate(++idx))
          .then(() => resolve(true))
          .catch((err) => reject(err))
      });
    };

    return Promise.resolve(true)
      .then(() => allDirCreate());
  }

  statMkdir(dirPath) {
    return new Promise((resolve, reject) => {
      fs.stat(dirPath, (err, stat) => {
        if (err) {
          // Dir not exists -> create dir
          this.mkdirAsync(dirPath)
            .then(()     => resolve(true))
            .catch((err) => reject(err));
          return;
        }
        // Dir exists
        resolve(true);
      });
    });
  }

  statFile(filePath) {
    return new Promise((resolve, reject) => {
      fs.stat(filePath, (err, stat) => {
        if (err) {
          // Dir not exists
          return reject(err);
        }
        // Dir exists
        resolve(true);
      });
    });
  }

  mkdirAsync(dirPath) {
    return new Promise((resolve, reject) => {
      fs.mkdir(dirPath, (err) => {
        if (err) {
          return reject(false);
        }
        resolve(true);
      });
    });
  }
}

module.exports = new StatMkdir();
