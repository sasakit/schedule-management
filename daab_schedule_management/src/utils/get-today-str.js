'use strict';

const getTodayString = () => {
  const today = new Date();
  const weekDayObj = ['日', '月', '火', '水', '木', '金', '土' ];
  return `${ today.getFullYear() }/${ today.getMonth()+1 }/${ today.getDate() }(${ weekDayObj[today.getDay()] })`;
}

module.exports = getTodayString;