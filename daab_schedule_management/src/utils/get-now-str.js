'use strict';

const zeroPadding = (val) => `0${ val }`.slice(-2);


const getNowString = () => {
  const today = new Date();
  return `${ today.getFullYear() }/${ today.getMonth()+1 }/${ today.getDate() } ${ zeroPadding(today.getHours()) }:${ zeroPadding(today.getMinutes()) }`;
}

module.exports = getNowString;
