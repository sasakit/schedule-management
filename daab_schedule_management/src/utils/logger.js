'use strict';

const Logs = require('log4js');
Logs.configure('log-config.json');

const debugLogger = Logs.getLogger('debug');
const errorLogger = Logs.getLogger('error');

const logger = {
  debug: (msg) => debugLogger.debug(msg),
  trace: (msg) => debugLogger.trace(msg),
  error: (msg) => errorLogger.error(msg)
};

module.exports = logger;
