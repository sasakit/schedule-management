'use strict';

const zeroPadding = (val) => `0${ val }`.slice(-2);

const getDateTimeString = (date = new Date()) => {
  const weekDayObj = ['日', '月', '火', '水', '木', '金', '土' ];

  return {
    year:        `${ date.getFullYear() }`,
    month:       `${ zeroPadding(date.getMonth()+1) }`,
    date:        `${ zeroPadding(date.getDate()) }`,
    day:         `${ date.getDay() }`,
    hour:        `${ zeroPadding(date.getHours()) }`,
    minutes:     `${ zeroPadding(date.getMinutes()) }`,
    second:      `${ zeroPadding(date.getSeconds()) }`,
    jpn_weekday: `${ weekDayObj[date.getDay()] }`,
  }
}


module.exports = getDateTimeString;
