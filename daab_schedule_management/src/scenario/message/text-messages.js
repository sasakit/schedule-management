'use strict';

require('dotenv').config();

const g = require('../../config');
const path = require('path');

// const deleteUserFiles = require('../../utils/delete-user-files');
const getNowString = require('../../utils/get-now-str');
// const botName = g.BOTNAME;
const botName = process.env.BOT_NAME? process.env.BOT_NAME: g.BOTNAME;

class TextMessages {

  constructor({ sendFunc }) {
    this.sendFunc = sendFunc;
  }

  joinRoom({ roomId }) {
    let message = [
      `私は${ botName }です。`,
      '利用を開始するには、何でも良いのでスタンプを送信して下さい。'
    ].join('\n');
    if ( process.env.MODE_KEY === g.MODE_LAST) {
      message = [
        message,
        g.MODE_LAST_MESSAGE
      ].join('\n');
    }
    const send = {
      text: message
    };
    this.sendFunc({ roomId, send });
  }

  joinRoomGroup({ roomId }) {
    let message = `私は${ botName }です。`;

    if ( process.env.MODE_KEY === g.MODE_LAST) {
      message = [
        message,
        g.MODE_LAST_MESSAGE
      ].join('\n');
    }
    else {
      message = [
        message,
        'グループトークではグループトークに入っている人の書き込みが表示されます。'
      ].join('\n');

    }

    const send = {
      text: message
    };
    this.sendFunc({ roomId, send });
  }

  byeRoom({ roomId }) {
    const send = {
      text: 'ご利用ありがとうございました。\n退室します。'
    };
    this.sendFunc({ roomId, send });
  }

  deleteSchedule({ roomId }) {
    const send = {
      text: '書き込みを削除しました。'
    };
    this.sendFunc({ roomId, send });
  }

  addSchedule({ roomId }) {
    const send = {
      text: '書き込みを登録しました。'
    };
    this.sendFunc({ roomId, send });
  }

  allClear({ roomId }) {
    const send = {
      text: 'すべてのデータを削除して、初期状態に戻しました。'
    };
    this.sendFunc({ roomId, send });
  }

  usage({ roomId }) {
    const send = {
      text:   `【${ botName }の使用方法】\n書き込む場合は、テキストで入力します。\n既に書き込まれている場合は、上書きされます。\n先頭に'++'を付けると、書き込みを追加することができます。\n書き込みを削除する場合は、「削除」を選びます。\n書き込みを変更すると書き込んだ方とボットを含むグループトークに通知されます。  `
    };
    this.sendFunc({ roomId, send });
  }

  // 更新時の一覧出力
  schduleOutput({ roomId, userId, userName, users, allObj }) {
    // users配列のemailをユーザアカウントとして検索？
    const dateStr = getNowString();
    const _users = users.map(item => item.id);
    const _scheduleList = _users.map( id => {
      if (id in allObj){
        const act = allObj[id].hasOwnProperty('act')? allObj[id].act : null;
        const name = allObj[id].hasOwnProperty('userName')? allObj[id].userName : null;
        const schedule = allObj[id].hasOwnProperty('schedule')? allObj[id].schedule : null;
        const modified = allObj[id].hasOwnProperty('modified')? allObj[id].modified : null;
        if(act) {
          if (!schedule) return; // 登録無しの場合は表示しない
          const _text0 = modified? modified: '登録なし';
          // const _text1 = schedule? schedule: '登録なし';
          const _text1 = schedule;
          const _text2 = _text1.replace(/<br>/g, '\n').trim();
          // const _text = '●' + name + '\n' + _text2;
          const _text = '--------' + '\n' + '【' + name + '】'+ '\n' + '（更新日時：' + _text0 + ')\n' + _text2;
          return _text;
        }
      }
    })

    const scheduleList = _scheduleList.filter(x => x);
    const base =  [
      `(${ dateStr })`,
      `【${ userName }】から通知がありました。`
    ].join("\n");

    let fulltext, subtext, scheduleText, send;
    fulltext = base;
    scheduleList.forEach((txt) => {
      subtext = [
        `${ fulltext }`,
        `${ txt }`
      ].join("\n");

      if(subtext.length > g.MAXTALKSTRINGS){
        send = { text: fulltext };
        this.sendFunc({ roomId, send });
        //
        fulltext = [
          `${ base }`,
          `${ txt }`
        ].join("\n");;
      }
      else {
        fulltext = subtext;
      }
    });

    send = { text: fulltext };
    this.sendFunc({ roomId, send });
  }


  // 更新時の後優先出力
  schduleOutputLastPriority({ roomId, userId, userName, users, allObj }) {
    const dateStr = getNowString();
    const _users = users.map(item => item.id);

    let _resultId, _modified, _modifiedSub, _schedule, _modifiedData, _modifiedSubData;
    for (let id in allObj) {
      _modifiedSub = allObj[id].hasOwnProperty('modified')? allObj[id].modified : null;
      _schedule = allObj[id].hasOwnProperty('schedule')? allObj[id].schedule : null;

      if ( _modifiedSub && _schedule ) {
        if ( _resultId ) {
          _modifiedSubData = new Date(_modifiedSub);
          if( _modifiedData.getTime() < _modifiedSubData.getTime()) {
            _resultId = id;
            _modifiedData = _modifiedSubData;
          }
        }
        else {
          _resultId = id;
          _modifiedData = new Date(_modifiedSub);
        }
      }
    }
    //
    let _text = '登録なし';
    if ( _resultId ) {
      const act = allObj[_resultId].hasOwnProperty('act')? allObj[_resultId].act : null;
      const name = allObj[_resultId].hasOwnProperty('userName')? allObj[_resultId].userName : null;
      const schedule = allObj[_resultId].hasOwnProperty('schedule')? allObj[_resultId].schedule : null;
      const modified = allObj[_resultId].hasOwnProperty('modified')? allObj[_resultId].modified : null;
      if(act) {
        const _text0 = modified? modified: '登録なし';
        const _text1 = schedule;
        const _text2 = _text1.replace(/<br>/g, '\n').trim();
        _text = '--------' + '\n' + '（更新日時：' + _text0 + ')\n' + _text2;
        // _text = '--------' + '\n' + '【' + name + '】'+ '\n' + '（更新日時：' + _text0 + ')\n' + _text2;
      }
    }

    const fulltext = [
      `(${ dateStr })`,
      `【${ userName }】から通知がありました。`,
      `${ _text }`
    ].join("\n");

    const send = { text: fulltext };
    this.sendFunc({ roomId, send });
  }

  // 出力要求による一覧出力
  schduleRequestOutput({ roomId, users, allObj }) {
    // users配列のemailをユーザアカウントとして検索？
    const dateStr = getNowString();
    const _users = users.map(item => item.id);
    const _scheduleList = _users.map( id => {
      if (id in allObj){
        const act = allObj[id].hasOwnProperty('act')? allObj[id].act : null;
        const name = allObj[id].hasOwnProperty('userName')? allObj[id].userName : null;
        const schedule = allObj[id].hasOwnProperty('schedule')? allObj[id].schedule : null;
        const modified = allObj[id].hasOwnProperty('modified')? allObj[id].modified : null;
        if(act) {
          if (!schedule) return; // 登録無しの場合は表示しない
          const _text0 = modified? modified: '登録なし';
          // const _text1 = schedule? schedule: '登録なし';
          const _text1 = schedule;
          const _text2 = _text1.replace(/<br>/g, '\n').trim();
          const _text = '--------' + '\n' + '【' + name + '】'+ '\n' + '（更新日時：' + _text0 + ')\n' + _text2;

          return _text;
        }
      }
    })

    const scheduleList = _scheduleList.filter(x => x);
    const base =  [
      `(${ dateStr })`
    ].join("\n");

    let fulltext, subtext, scheduleText, send;

    fulltext = base;
    scheduleList.forEach((txt) => {
      subtext = [
        `${ fulltext }`,
        `${ txt }`
      ].join("\n");

      if(subtext.length > g.MAXTALKSTRINGS){
        send = { text: fulltext };
        this.sendFunc({ roomId, send });
        //
        fulltext = [
          `${ base }`,
          `${ txt }`
        ].join("\n");;
      }
      else {
        fulltext = subtext;
      }
    });

    send = { text: fulltext };
    this.sendFunc({ roomId, send });

  }


  // 出力要求による後優先出力
  schduleRequestOutputLastPriority({ roomId, users, allObj }) {
    const dateStr = getNowString();
    const _users = users.map(item => item.id);

    let _resultId, _modified, _modifiedSub, _schedule, _modifiedData, _modifiedSubData;
    for (let id in allObj) {
      _modifiedSub = allObj[id].hasOwnProperty('modified')? allObj[id].modified : null;
      _schedule = allObj[id].hasOwnProperty('schedule')? allObj[id].schedule : null;

      if ( _modifiedSub && _schedule ) {
        if ( _resultId ) {
          _modifiedSubData = new Date(_modifiedSub);
          if( _modifiedData.getTime() < _modifiedSubData.getTime()) {
            _resultId = id;
            _modifiedData = _modifiedSubData;
          }
        }
        else {
          _resultId = id;
          _modifiedData = new Date(_modifiedSub);
        }
      }
    }

    let _text = '登録なし';
    if ( _resultId ) {
      const act = allObj[_resultId].hasOwnProperty('act')? allObj[_resultId].act : null;
      // const name = allObj[_resultId].hasOwnProperty('userName')? allObj[_resultId].userName : null;
      const schedule = allObj[_resultId].hasOwnProperty('schedule')? allObj[_resultId].schedule : null;
      const modified = allObj[_resultId].hasOwnProperty('modified')? allObj[_resultId].modified : null;
      if (act) {
        const _text0 = modified? modified: '登録なし';
        const _text1 = schedule;
        const _text2 = _text1.replace(/<br>/g, '\n').trim();
        // _text = '--------' + '\n' + '【' + name + '】'+ '\n' + '（更新日時：' + _text0 + ')\n' + _text2;
        _text = '--------' + '\n' + '（更新日時：' + _text0 + ')\n' + _text2;
      }
    }

    const fulltext = [
      `(${ dateStr })`,
      `${ _text }`
    ].join("\n");

    const send = { text: fulltext };
    this.sendFunc({ roomId, send });
  }

  error({ roomId, msg }) {
    const send = { text: `[Error]\n${ msg }` };
    this.sendFunc({ roomId, send });
  }

  alertInPreparetion({ roomId }) {
    const send = {
      text: 'この機能は準備中です'
    };
    this.sendFunc({ roomId, send });
  }

  alertOverMaxInput({ roomId }) {
    const send = {
      text: '入力文字は900文字以内です\n入力し直してください'
    };
    this.sendFunc({ roomId, send });
  }

}

module.exports = TextMessages;
