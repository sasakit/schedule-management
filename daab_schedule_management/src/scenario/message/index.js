'use strict';


class Message {

  constructor(components) {
    components.mixin(this, __dirname, {
      Message : './text-messages',
      question: './question-messages',
      nextMessage: './next-message',    // 次のメッセージ表示
    });
  }

}

module.exports = Message;
