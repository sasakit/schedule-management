'use strict';

// const strToday = require('../../utils/get-today-str');
const strNow = require('../../utils/get-now-str');

const questionMessages = () => (act, value) => {

  const msg = value.replace(/<br>/g, '\n').trim();

  const MESSAGES = {

    'HOME': {
      question: `【自分の書き込み】\n${ msg }\nーーー\nテキストで入力して下さい。\n追記する場合は、先頭に'++'を付けて下さい\n削除する場合は、「削除」を選んで下さい。`,
      options : [
        '削除',
        // 'ボット退室'
      ]
    },

  };


  // testにマッチする対象を返す
  const findMessage = (questionMessage) => {
    const messageKeys = Object.keys(MESSAGES);
    const questions = messageKeys.map((key) => ({ act: key, question: MESSAGES[key].question }));
    return questions.find((el, idx) => {
      const reg = new RegExp(el.question);
      return reg.test(questionMessage);
    });
  };

  // testでマッチした対象のexecの結果を返す
  const findOptions = (questionMessage) => {
    const message = findMessage(questionMessage);
    const reg = new RegExp(message.question);
    return reg.exec(questionMessage);
  };


  return {
    get selectMsg() { return MESSAGES[act]; },
    get list() { return MESSAGES; },
    findMessage(questionMessage) { return findMessage(questionMessage); },
    findOptions(questionMessage) { return findOptions(questionMessage); }
  }
};
module.exports = questionMessages;
