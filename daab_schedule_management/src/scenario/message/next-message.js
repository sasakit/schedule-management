'use strict';

const g    = require('../../config');

// 次のメッセージの表示処理
const nextMessage = ({ response, message, sendFunc }) => {
  return ({ roomId, res, next, scheduleStatus }) => {

    const act = next;
    const itemStage = g.SCHEDULE_STAGE_ITEM;

    // 次のステージの値を取得（登録されていれば）
    const currentSchedule = scheduleStatus[itemStage[act]];
    const value = currentSchedule? currentSchedule: '登録なし';

    sendFunc({ roomId, send: message.question(act, value).selectMsg });
  };
};

module.exports = nextMessage;
