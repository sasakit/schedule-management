'use strict';

// const convertSingleRowString = (str) => str.replace(/\r\n/g, ' ').replace(/(\n|\r)/g, ' ').trim();
const convertSingleRowString = (str) => str.replace(/\r\n/g, '<br>').replace(/(\n|\r)/g, '<br>').trim();

const textAction = ({ scenario, schedules, record, message, response, join, sendFunc, logger }) => {
  return ({ res, msg, act, scheduleStatus }) => {
    // ログ出力
    const { roomId, userId } = response.getUserVariousInfo(res);
    logger.debug(`TextAction      ROOM:${ roomId }  User:${ userId }  Act:${ act }  Message:${ msg }`);

    const roomType = response.getRoomType(res);
    // グループトークではテキスト入力をスルーする
    if(roomType === 'group') return;

    const str = convertSingleRowString(msg);
    // ヘルプ
    if( str === '#h' || str === '#help' || str === '#使い方') {
      message.usage({ roomId });
      return;
    }


    // すべて削除
    // if( str === '#allclear' ) {
    //   record.allClear({ res, roomId, userId });
    //   // 最初のメッセージを送信
    //   message.allClear({ roomId })
    //   sendFunc({ roomId, send: message.question('HOME').selectMsg });
    //   return;
    // }

    const addPattern = '++';
    let msgString = msg;
    // 追記の場合
    if (!msg.indexOf(addPattern)) {
      const scheduleStatus = schedules.loadOne({ userId });
      const currentSchedule = scheduleStatus['schedule'];
      if (currentSchedule) msgString = currentSchedule + '\n' + msg.substr(2);
      else msgString = msg.substr(2);
    }

    switch (act) {
      // 登録文字列
      case 'HOME': {
        // record.textSchedule({ res, msg: convertSingleRowString(msg) });
        record.textSchedule({ res, msg: convertSingleRowString(msgString) });
        break;
      }

      default: {
        // 「ボット」「使い方」が入力されていたら初回メッセージを送る
        if (msg.includes('ボット') || msg.includes('使い方')) {
          // 最初のメッセージ送信
          join.home({ res, roomId: response.getRoomId(res) });
        }
      };
    }
  };
};

module.exports = textAction;
