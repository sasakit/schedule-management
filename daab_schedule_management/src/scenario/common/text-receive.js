'use strict';

const _ = require('lodash');


// [テキスト受信]
const textReceive = ({ response, schedules, scenario, message, logger }) => {
  return ({ res, msg }) => {

    const { roomId, userId, userName, userAccount } = response.getUserVariousInfo(res);
    const modified = new Date();

    // 報告書の新規登録・更新・情報取得
    // const scheduleStatus = schedules.update({ key: { userId } });
    const scheduleStatus = schedules.loadOne({ userId });

    const { act } = scheduleStatus;

    // 処理対象別処理
    scenario.textAction({ res, msg, act, scheduleStatus });

  };
};

module.exports = textReceive;
