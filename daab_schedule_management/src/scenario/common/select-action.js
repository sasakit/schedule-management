'use strict';

const _ = require('lodash');
const strToday = require('../../utils/get-today-str');
const strNow = require('../../utils/get-now-str');

// セレクトスタンプ返信対象別処理
const selectAction = ({ response, message, record, logger }) => {
  return ({ res, act, scheduleStatus }) => {
    // 選択した選択肢番号(0 ~)
    const selectStamp = response.getSelectStampRes(res);
    const selectedIdx = _.get(selectStamp, 'response');
    const mes = res.json.options[selectedIdx];

    // ログ出力
    const { roomId, userId } = response.getUserVariousInfo(res);
    logger.debug(`SelectAction    ROOM:${ roomId }  User:${ userId }  Act:${ act }  SelectId:${ selectedIdx }`);

    // Q. 報告書を作成開始する？
    if (act === 'HOME') {
      // A. 削除
      if (mes.indexOf('削除') != -1) {
        record.selectDeleteSchedule({ res });
      }

      return;
    }

  };
};

module.exports = selectAction;
