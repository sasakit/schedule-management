'use strict';

const _ = require('lodash');
require('dotenv').config();

// [セレクトスタンプ返信受信]
const stampReceive = ({ response, schedules, scenario, message, logger }) => {
  return (res) => {
    scenario.stampAction({ res });
  };
};

module.exports = stampReceive;
