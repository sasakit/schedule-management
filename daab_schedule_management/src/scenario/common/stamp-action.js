'use strict';

const _ = require('lodash');
const strToday = require('../../utils/get-today-str');
const strNow = require('../../utils/get-now-str');

// セレクトスタンプ返信対象別処理
const stampAction = ({ response, message, record, join, logger }) => {
  return ({ res }) => {
    // ログ出力
    const { roomId, userId } = response.getUserVariousInfo(res);
    logger.debug(`StampAction    ROOM:${ roomId }  User:${ userId } `);

    const roomType = response.getRoomType(res);
    // グループトークではテキスト入力をスルーする
    switch (roomType) {
        case 'pair':
          // ペアトークに最初のメッセージ
          join.home({ res, roomId });
          break;
        case 'group':
          // グループトークに登録状況の表示
          record.reListSchedule({ res, roomId });
          break;
        default:
          break;
    }

  };
};

module.exports = stampAction;
