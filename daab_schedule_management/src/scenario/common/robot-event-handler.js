'use strict';

const g = require('../../config');

module.exports = ({ robot, hearMessage, response, scenario, join, message, logger }) => {

  // テキスト受信
  const textFunc = (res) => {
    const msg = hearMessage(res);
    if (msg === false) return;

    scenario.textReceive({ res, msg });
  };

  // セレクトスタンプ受信
  const selectFunc = (res) => {
    const replyMessageId = response.getSelectReplyMessageId(res);

    // セレクトスタンプへの返信の場合
    if (!!replyMessageId) {
      scenario.selectReceive(res);
      return;
    }
  };

  // スタンプ受信
  const stampFunc = (res) => {
    scenario.stampReceive(res);

    // const { roomId } = response.getUserVariousInfo(res);
    // // 最初のメッセージ
    // join.home({ res, roomId });
  };


  return () => {
    // 自分が参加
    robot.join((res) => process.nextTick(() => join.own(res)));

    robot.hear(/([\s\S]+)/, (res) => process.nextTick(() => textFunc(res)));
    robot.hear('select',    (res) => process.nextTick(() => selectFunc(res)));
    robot.hear('stamp',     (res) => process.nextTick(() => stampFunc(res)));
  };
};
