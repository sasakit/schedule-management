'use strict';

const _ = require('lodash');
require('dotenv').config();

const getNowString = require('../../utils/get-now-str');

// [セレクトスタンプ返信受信]
const selectReceive = ({ response, schedules, scenario, message, logger }) => {
  return (res) => {
    const branch = process.env.BRANCHNAME;

    const { roomId, userId, userName, userAccount } = response.getUserVariousInfo(res);
    const selectStamp = response.getSelectStampRes(res);

    // ステージ(act)の取得
    // const modified = new Date();
    const modified = getNowString();

    const scheduleStatus = schedules.update({ key: { userId }, save: { userName, userAccount, modified } });
    const { act } = scheduleStatus;

    // 返答対象が見つからなかった場合
    if (!act) {
      logger.debug(`ROOM:${ roomId }  User:${ userId }  Act:${ act }　Question not found`);
      res.send('質問が見つかりませんでした。ボットが送信したメッセージへの返信をお願いします。\n改善しない場合は、一度、トークメニューで「トークから退出」して下さい。');
      return;
    }
    else {
      // 返答対象別処理
      scenario.selectAction({ res, act, scheduleStatus });
    }

  };
};

module.exports = selectReceive;
