
class Common {

  constructor(components) {
    components.mixin(this, __dirname, {
      robotEventHandler: './robot-event-handler',
      selectReceive    : './select-receive',
      selectAction     : './select-action',
      textReceive      : './text-receive',
      textAction       : './text-action',
      stampReceive      : './stamp-receive',
      stampAction       : './stamp-action',
    });
  }

}

module.exports = Common;
