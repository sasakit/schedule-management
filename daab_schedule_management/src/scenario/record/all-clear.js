'use strict';

const g    = require('../../config');
const getNowString = require('../../utils/get-now-str');

const allClear =({ record, message, response, schedules, logger }) => {
  return ({ res, roomId, userId }) => {
    logger.debug(`All Clear       ROOM:${ roomId }  User:${ userId } `);

    const { userName, userAccount } = response.getUserVariousInfo(res);

    schedules.clear({ userId });
    const act = 'HOME';
    const mode = g.WORK_MODE[0];
    // const modified = new Date();
    const modified = getNowString();
    schedules.update({ key: { userId }, save: { act, userName, userAccount, mode, modified } });
  };
};

module.exports = allClear;
