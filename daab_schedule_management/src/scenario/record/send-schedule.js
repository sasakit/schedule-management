'use strict';

require('dotenv').config();

const g = require('../../config');

// グループへのメッセージ送信
const sendSchedule =  ({  response, schedules, message, sendFunc, join, logger }) => {
  return ({ res, roomId, userId, userName, userAccount }) => {

    let noRooms = false; // 通知するグループトークがない場合にtrueにする

    // グループトーク（type : 2）、かつ、報告者が含まれているグループトーク
    const _roomsGrouptalk = Object.entries(res.message.rooms).filter(([id, talk]) =>  talk.type === 2 );

    const _rooms = _roomsGrouptalk.filter( function( [id, talk] ) {
      let _value = false;
      talk.users.forEach( (user) => {
        if (user.name === userName) _value = true;
      });
      return _value;
    });

    if( !_rooms.length ) { // 通知するグループトークがない場合
      noRooms = true;
      sendFunc({ roomId, send: { text:'通知するグループトークが見つかりませんでした、グループトークの登録を確認して下さい。'} });
      return
    }

    const allObj = schedules.brainGet();
    // グループトークへメッセージ送信
    if ( process.env.MODE_KEY === g.MODE_LAST) {
      _rooms.map(([id, talk]) => message.schduleOutputLastPriority({ roomId: id, userId, userName, users: talk.users, allObj}));
    }
    else {
      _rooms.map(([id, talk]) => message.schduleOutput({ roomId: id, userId, userName, users: talk.users, allObj}));
    }
    _rooms.map(([id, talk]) => logger.debug(`Send Report   ROOM:${ id }  User:${ userId }`));
    return;

  };
};

module.exports = sendSchedule;
