'use strict';


class Record {

  constructor(components) {
    components.mixin(this, __dirname, {
      textSchedule : './text-schedule',
      selectDeleteSchedule : 'select-delete-schedule',
      sendSchedule : 'send-schedule',
      reListSchedule : 'relist-schedule',
      textProc : './text-proc',
      allClear : './all-clear'
    });
  }

}

module.exports = Record;
