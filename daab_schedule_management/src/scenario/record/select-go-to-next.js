'use strict';

const g    = require('../../config');

// 指定されたアクションに更新する
const selectGoToNext = ({ response, schedules, message, sendFunc }) => {
  return ({ res, act }) => {

    const itemStage = g.SCHEDULE_STAGE_ITEM;

    const { roomId, userId, userName, userAccount } = response.getUserVariousInfo(res);
    const modified = new Date();


    // 更新情報セット
    // const scheduleStatus = schedules.update({ key: { userId }, save: { act, userName, userAccount, modified } });

    const scheduleStatus = schedules.loadOne({ userId });

    message.nextMessage({ roomId, res, next: act, scheduleStatus });

  };
};

module.exports = selectGoToNext;
