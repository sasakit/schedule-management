'use strict';

const g    = require('../../config');
const getNowString = require('../../utils/get-now-str');

// テキスト返答処理
const textProc = ({ response, schedules, message, sendFunc }) => {
  return ({ res, now, next, msg }) => {
    // now:現在のステージ
    // next:次のステージ

    const act = next;
    const itemStage = g.SCHEDULE_STAGE_ITEM;

    const { roomId, userId, userName, userAccount } = response.getUserVariousInfo(res);
    // const modified = new Date();
    const modified = getNowString();

    // 更新情報セット
    const scheduleStatus = schedules.update({ key: { roomId }, save: { act, userName, userAccount, [itemStage[now]]: msg, modified } });

    message.nextMessage({ roomId, res, next, scheduleStatus });

  };
};

module.exports = textProc;
