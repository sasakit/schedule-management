'use strict';

const g    = require('../../config');
const getNowString = require('../../utils/get-now-str');

// 書き込みの削除
const selectDeleteSchedule = ({ response, schedules, sendFunc, message, record, logger }) => {
  return ({ res }) => {
    const { roomId, userId, userName, userAccount } = response.getUserVariousInfo(res);

    logger.debug(`Delete    ROOM:${ roomId }  User:${ userId }`);

    const status = schedules.loadOne({ userId });

    // 登録された書き込みの削除
    schedules.delete({ userId, deleteOneKey: 'schedule' });

    //
    const act = 'HOME';
    // const modified = new Date();
    const modified = getNowString();
    // 更新情報セット
    const scheduleStatus = schedules.update({ key: { userId }, save: { act, modified } });

    // グループトークへの通知
    record.sendSchedule({ res, scheduleStatus, roomId, userId, userName, userAccount });

    // 「削除完了」メッセージ送信
    message.deleteSchedule({ roomId })

    // 次のメッセージの表示
    message.nextMessage({ roomId, res, next:act, scheduleStatus });
  };
};

module.exports = selectDeleteSchedule;
