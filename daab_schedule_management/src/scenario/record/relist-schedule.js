'use strict';

const g = require('../../config');

// グループへのメッセージ送信（全員の書き込みを表示する）
const reListSchedule =  ({  response, schedules, message, sendFunc, logger }) => {
  return ({ res, roomId }) => {

    const roomUsers = response.getRoomUsers(res);
    const allObj = schedules.brainGet();


    // グループトークへメッセージ送信
    if ( process.env.MODE_KEY === g.MODE_LAST) {
      // 最新の書き込みのみを表示する
      message.schduleRequestOutputLastPriority({ roomId, users: roomUsers, allObj});
    }
    else {
      // 全員の書き込みを表示する
      message.schduleRequestOutput({ roomId, users: roomUsers, allObj});
    }
    return;
  };
};

module.exports = reListSchedule;
