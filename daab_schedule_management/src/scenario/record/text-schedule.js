'use strict';

const g    = require('../../config');
const getNowString = require('../../utils/get-now-str');

// 入力：テキスト返答処理
const textSchedule = ({ response, schedules, message, join, record, sendFunc }) => {
  return ({ res, msg }) => {
     // 次のアクション
    const act = 'HOME';
    const { roomId, userId, userName, userAccount } = response.getUserVariousInfo(res);
    // const modified = new Date();
    const modified = getNowString();

    if(msg.length > g.MAXINPUTSTRINGS){
      message.alertOverMaxInput({ roomId });
      return;
    }

    // 更新情報セット
    const scheduleStatus = schedules.update({ key: { userId }, save: { act, userName, userAccount, schedule: msg, modified } });

    // const scheduleStatus = schedules.loadOne({ userId });


    // 「登録完了」メッセージ送信
    message.addSchedule({ roomId })

    // グループトークへの通知
    record.sendSchedule({ res, roomId, userId, userName, userAccount });

    // 次のメッセージの表示
    message.nextMessage({ roomId, res, next:act, scheduleStatus });
  };
};

module.exports = textSchedule;
