'use strict';

const g    = require('../../config');

// HOME: 最初のメッセージ
const home = ({ response, schedules, message, sendFunc }) => {
  return ({ res, roomId }) => {

    const roomType = response.getRoomType(res);
    // グループトークではスルーする
    if(roomType === 'group') return;

    // 参加直後のステージ(HOME)を設定
    const act = 'HOME';
    const modified = new Date();
    const { userName, userId } = response.getUserVariousInfo(res);

    // const status = schedules.loadOne({ userId });

    // 更新情報セット
    const scheduleStatus = schedules.update({ key: { userId }, save: { act, userName } });
    const { schedule } = scheduleStatus;

    const currentSchedule = schedule? schedule: '登録なし';

    return sendFunc({ roomId, send: message.question('HOME', currentSchedule).selectMsg });
  };
};

module.exports = home;
