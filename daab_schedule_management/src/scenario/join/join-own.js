'use strict';


const joinFunc = ({ response, message, join, logger }) => {
  return (res) => {
    const roomId = response.getRoomId(res);
    const roomType = response.getRoomType(res);

    // ログ出力
    logger.debug(`ROOM:${ roomId }  TYPE:${ roomType }  JOIN`);

    if(roomType === 'pair') { // ペアトーク
      // トーク参加時のメッセージ
      message.joinRoom({ roomId });

    } else if(roomType === 'group') { // グループトーク
      // トーク参加時のメッセージ
      message.joinRoomGroup({ roomId });
    }
  };
};

module.exports = joinFunc;
