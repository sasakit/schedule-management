'use strict';


class Join {

  constructor(components) {
    components.mixin(this, __dirname, {
      own : './join-own',
      home: './home',
    });
  }

}

module.exports = Join;
