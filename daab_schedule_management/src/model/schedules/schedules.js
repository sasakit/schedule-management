'use strict';

const KEY_REDIS = 'schedules';

const g = require('../../config');
const _ = require('lodash');


class Schedules {

  constructor({ robot, logger }) {
    this.brain = robot.brain;
    this.logger = logger;
  }

  brainGet() {
    return this.brain.get(KEY_REDIS) || {};
  }

  brainSet({ saveAllObj }) {
    if (!_.isObject(saveAllObj)) {
      throw new TypeError('recorders.brainSet');
    }

    this.brain.set(KEY_REDIS, saveAllObj);
    this.brain.save();
  }

  // 上書セーブ
  save({ userId, saveOneObj }) {
    if (!userId) return false;

    const allObj = this.brainGet();
    const key    = `${ userId }`;
    allObj[key]  = saveOneObj;

    // Logger
    this.logger.debug(`Record Save   USERID:${ userId }`);

    this.brainSet({ saveAllObj: allObj });
  }

  // 指定データ取得
  loadOne({ userId }) {
    if (!userId) return false;

    const allObj = this.brainGet();
    const key    = `${ userId }`;
    return allObj[key] || {};
  }

  // 全データ取得
  loadAll() {
    return this.brainGet();
  }

  // 新規登録、更新、更新後情報を返す
  update({ key, save }) {
    const { userId = false } = key;

    if (!userId ) return false;

    const oneObj = this.loadOne(key);
    const saveOneObj = Object.assign({}, oneObj, save);

    // セーブ処理は非同期させる（待たない）
    //process.nextTick(() => this.save({ roomId, userId, saveOneObj }));
    this.save({ userId, saveOneObj });

    return saveOneObj;
  }

  // クリア
  clear({ userId }) {
    if (!userId ) return false;

    const allObj = this.brainGet();
    const key    = `${ userId }`;
    delete allObj[key];

    // Logger
    this.logger.debug(`Record Clear   USERID:${ userId }`);

    this.brainSet({ saveAllObj: allObj });
  }

  delete({ userId, deleteOneKey }) {
    if (!userId ) return false;

    const allObj = this.brainGet();
    const key    = `${ userId }`;
    const deletekey = deleteOneKey;

    delete allObj[key][deletekey];

    // Logger
    this.logger.debug(`Record delete:${ deletekey }   USERID:${ userId }`);
    this.brainSet({ saveAllObj: allObj });
  }

}

module.exports = Schedules;
