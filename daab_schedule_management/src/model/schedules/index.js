'use strict';

class Schedules {

  constructor(components) {
    components.mixin(this, __dirname, {
      Schedules        : './schedules',
    });
  }

}

module.exports = Schedules;
