const g = {};

// module(system)
const path = require('path');

g.BOTNAME = 'ホワイトボードボット';

// JSON/FILE保存先
g.PATH_LOGS          = path.resolve('./logs');
// g.PATH_SAVE_FILES    = path.resolve('./save-files');

// ステージとDB要素
g.SCHEDULE_STAGE_ITEM = {
  'HOME' : 'schedule',
};

g.MAXTALKSTRINGS = 1024;
g.MAXINPUTSTRINGS = 900;

g.MODE_KEY = 'MODE_KEY';
g.MODE_LAST = 'LAST-PRIORITY';

g.MODE_LAST_MESSAGE = 'このボットは、グループトークでは「後優先」で表示しますので、ご注意ください。';

module.exports = g;
