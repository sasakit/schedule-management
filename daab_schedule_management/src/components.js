'use strict';

const core = (robot) => ({
  // hubot
  robot,
  brain: robot.brain,

  // daab
  sendFunc:    './daab/send-func',
  hearMessage: './daab/hear-message',
  response:    './daab/response',

  // model
  schedules:  './model/schedules',

  // scenario
  scenario: './scenario/common',
  join:     './scenario/join',
  message:  './scenario/message',
  record:   './scenario/record',

  // module
  logger:   './utils/logger',

  // main
  main: './main',
});

const Components = require('./utils/components');

module.exports = (robot) => new Components(__dirname, core(robot));
