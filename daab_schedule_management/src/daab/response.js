'use strict';

const _ = require('lodash');

class UtilRes {

  // ユーザID
  getUserId(res) {
    return _.get(res, 'message.user.id');
  }

  // ユーザ名
  getUserName(res) {
    return _.get(res, 'message.user.name');
  }

  // ユーザemail
  getUserEmail(res) {
    return _.toLower(_.get(res, 'message.user.email'));
  }

  // ルームID
  getRoomId(res) {
    return _.get(res, 'message.room');
  }
  
  // ユーザ情報
  getUserVariousInfo(res) {
    return {
      roomId     : this.getRoomId(res),
      userId     : this.getUserId(res),
      userName   : this.getUserName(res),
      userAccount: this.getUserEmail(res),
    };
  }

  // メッセージID
  getMessageId(res) {
    return _.get(res, 'message.id');
  }

  // トークルームの種類(ペア or グループ)
  getRoomType(res) {
    return _.get(res, 'message.roomType') === 1 ? 'pair' : 'group';
  }

  // セレクトスタンプの送信or返信
  getSelectSendResponse(res) {
    return _.isUndefined(_.get(res, 'json.response')) ? 'send' : 'response';
  }

  // セレクトスタンプ送信
  getSelectStamp(res) {
    const question = _.get(res, 'json.question');

    if (_.isUndefined(question)) {
      return false;
    }

    return _.get(res, 'json');
  }

  // セレクトスタンプ返信
  getSelectStampRes(res) {
    const question = _.get(res, 'json.question');
    const replyTo = _.get(res, 'json.in_reply_to');

    if (_.isUndefined(question) || _.isUndefined(replyTo)) {
      return false;
    }

    return _.get(res, 'json');
  }

  // セレクトスタンプの返信先メッセージID
  getSelectReplyMessageId(res) {
    return _.get(res, 'json.in_reply_to');
  }

  // ルーム内ユーザー情報
  getRoomUsers(res) {
    return _.get(res, 'message.roomUsers');
  }

  // ファイル情報
  getFile(res) {
    return _.get(res, 'json');
  }
}

module.exports = UtilRes;
