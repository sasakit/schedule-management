'use strict';

const _     = require('lodash');
const merge = require('merge');


// メッセージ送信処理 (複数roomId(array)対応)
const SendFunc = ({ robot }) => {
  return ({ roomId, send }) => {
    return new Promise((resolve, reject) => {

      if (!roomId) return resolve(false);

      const sendObject = merge.recursive(true, send, {
        onsend: (sent, msg) => resolve(`_${msg.id.high}_${msg.id.low}`)
      });

      if (_.isArray(roomId)) {
        Promise.all(_.map(roomId, (room) => robot.send({ room }, sendObject))).then(resolve).catch(reject);
      } else {
        robot.send({ room: roomId }, sendObject);
      }
    });
  };

}

module.exports = SendFunc;
