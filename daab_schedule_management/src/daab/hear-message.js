'use strict';


const hearMessage = (res) => {
  let msg = res.match[1];

  // ペアトークの場合
  if (res.message.roomType === 1) {
    msg = msg.replace(/^\S+ /, '');
  }

  if (/^\{[\s\S]+\}$/.test(msg)) return false;

  // トークルーム名の変更を対象外に
  if (res.message.id === null) return false;

  // スタンプ、セレクト、ファイル送信を対象外に
  if ((msg.includes('stamp_set') && msg.includes('stamp_index')) ||
      (msg.includes('response') || msg.includes('question')) ||
      (msg.includes('file_id'))) {
    return false;
  }


  // 入力されたテキストメッセージ内容
  return msg;
};

module.exports = () => hearMessage;
