// Description:
//   BOTレンタルのアカウントに合致するものがあるかどうかを判定し、合致すれば処理を終了
const preventBotTalk = (res) => {
    const regexp = /bot\+rental.*@lisb\.direct/;
    const notIncludeBot = res.message.roomUsers.every((user) => res.message.user.email === user.email || !regexp.test(user.email))

    if (!notIncludeBot) {
      res.send("既に、このトークルームではボットが動いているため、退出いたします。");
      res.leave();
      res.finish();
    }
};

// 確認処理を実行
module.exports = (robot) => {
  robot.join((res) => preventBotTalk(res));
}
