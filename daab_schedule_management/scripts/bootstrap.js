// Description:
//   daab_schedule_management
'use strict';

const components = require('../src/components');

module.exports = (robot) => {
  components(robot).main;
}
