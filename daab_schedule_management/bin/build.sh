  #!/bin/bash

# ddaab_schedule_management to daab_schedule_management{d}

echo "============== run build =============="

if [ -d ../daab_schedule_management ]; then
  echo "start rsync to..."
else
  echo "not found current dir."
  echo "=============    fail    =============="
  exit 0
fi

# 同期
for proj in "daab_schedule_management1" "daab_schedule_management2"
do
  echo "...$proj"

  cp -a ../daab_schedule_management/{package.json,external-scripts.json,log-config.json,bin,scripts,src} ../$proj/
  rsync -av --delete ../daab_schedule_management/{package.json,external-scripts.json,log-config.json,bin,scripts,src} ../$proj/ --exclude=.*
  rsync -av ../daab_schedule_management/src/.foreverignore ../$proj/src/
done

echo "rsync finish."
echo "============== success =============="
