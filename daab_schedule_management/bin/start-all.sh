#!/bin/bash

# start all bots
echo "============== start all bots =============="

# daab login
for proj in "daab_schedule_management1" "daab_schedule_management2"
do
  if [ ! -e ../$proj/.env ]; then
    echo "[$proj] daab login"
    cd ../$proj && daab login
    exit 0
  fi
done

# daab start
for proj in "daab_schedule_management1" "daab_schedule_management2"
do
  echo "[$proj] daab start -d"
  cd ../$proj && daab start -d
done

echo "run all bots finish."
echo "============== success =============="

# output logs
sleep 1s
tail -f ../daab_schedule_management1/hubot.log ../daab_schedule_management2/hubot.log
