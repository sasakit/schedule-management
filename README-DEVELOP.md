# ホワイトボードボット(開発用)


### npm run *



`npm run build`

複数ボットのディレクトリを用意するコマンド
`daab-schedule-management`をコピーして`daab-schedule-management{1, 2}`を作る。
コードの修正はcurrentで行うが、ボットの起動はdestで行う。



`npm run local`

(ローカル開発環境用) 複数ボットを同時起動するコマンド
`.env`が無い場合は`daab login`が行われる
強制終了(Ctrl+C)するとボットも停止させる
