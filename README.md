# ホワイトボードボット
仮実装版：2019/10/29

## 概要
「ホワイトボードボット」  

本ボットでは、利用者がボットとのペアトークで入力をおこない、利用者とボットを含むグループトークで、情報を共有することができる。  

## Require
Node.js v6~ (開発ではv8.11.3使用)  
Redis  

## ボットスクリプト
`daab-schedule-management/scripts/`  
`daab-schedule-management/src/`  


## ログファイル保存場所(自動作成)
`daab_schedule_management{1,2}/logs/debug.log`  
`daab_schedule_management{1,2}/logs/error.log`


## Redis保存内容
```
schedules: {
  "(ユーザーId)": {
    userName: '',
    userAccount: '',
    act: '',
    modified: '',
    schedule: '',
  }
}
```


## 実行方法
稼働させるボットのディレクトリを用意
```
$ cd daab_schedule_management
$ npm run build
```

ログイン
```
$ cd daab_schedule_management{1,2}
$ daab login
(ボットアカウント情報入力)
```

起動
```
$ daab start -d
```

停止
```
$ daab stop
```
